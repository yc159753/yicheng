from django.shortcuts import reverse
def index(request):
    return reverse(request, 'index')