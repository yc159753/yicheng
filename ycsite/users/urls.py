from django.urls import path ,re_path,register_converter

from users.views import users_list, users_detail

from . import views

class FourDigitYearConverter:
    regex = '[0-9]{4}'

    def to_python(self, value):
        return int(value)

    def to_url(self, value):
        return '%04d' % value

register_converter(FourDigitYearConverter, 'yyyy')
app_name = "users"

urlpatterns = [
    path('user_l/<path:uid>',views.users_list, name="usersCharge"),
    path('user_lis/<path:uid>',views.users_list, name="usersList"),
    path('user_li/<path:uid>',views.users_list, name="usersDetail"),
    # path('users_list/',views.users_list, name="usersList"),
    path('<int:uid>/', views.users_detail),
    # path('users/<int:uid>/', users_detail),
    # path('users/<slug:uid>/', users_detail),
    # path('users/<slug:name>/', users_list),
    # path('users/<yyyy:uid>/', users_detail),
    # re_path(r'^(?P<uid>\w+)/$',views.users_detail,name="users_detail"),
    # re_path(r'^users/(?P<name>\w+)/$',users_list)
]